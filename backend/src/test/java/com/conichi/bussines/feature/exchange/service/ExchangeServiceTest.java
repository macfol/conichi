package com.conichi.bussines.feature.exchange.service;

import com.conichi.bussines.feature.exchange.dto.ExchangeInquiryDto;
import com.conichi.bussines.feature.exchange.dto.ExchangeRateDto;
import com.conichi.bussines.feature.exchange.feed.ExchangeFeed;
import com.conichi.bussines.feature.exchange.model.ExchangeRate;
import com.conichi.bussines.feature.exchange.repository.ExchangeRateRepository;
import com.conichi.bussines.feature.shared.TimeValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ExchangeServiceTest {

    private static final String EUR = "EUR";
    private static final String CHF = "CHF";

    private static final ExchangeRate EXCHANGE_RATE = new ExchangeRate(1L, EUR, CHF, 0.5, LocalDateTime.now());
    private static final ExchangeInquiryDto EXCHANGE_INQUIRY_DTO = new ExchangeInquiryDto(EUR, CHF, 100.0);
    private static final ExchangeRateDto EXCHANGE_RATE_DTO = new ExchangeRateDto(EUR, CHF, 0.5);

    @Mock
    ExchangeFeed feed;
    @Mock
    ExchangeRateRepository exchangeRateRepository;
    @Mock
    TimeValidator timeValidator;
    @InjectMocks
    ExchangeService exchangeService;

    @Test
    public void shouldCallApiWhenCurrencyPairIsRequestedFirstTime() {
        //given
        when(exchangeRateRepository.findByFromCurrencyAndToCurrency(EUR, CHF)).thenReturn(Optional.empty());
        when(feed.getExchangeRate(EXCHANGE_INQUIRY_DTO)).thenReturn(EXCHANGE_RATE_DTO);
        when(exchangeRateRepository.save(ExchangeRateDto.parseFromDto(EXCHANGE_RATE_DTO)))
                .thenReturn(EXCHANGE_RATE);
        //when
        final Double result = exchangeService.getExchangeRate(EXCHANGE_INQUIRY_DTO);
        //then
        assertEquals(50.0, result, 0);
        verify(exchangeRateRepository, times(1)).save(any());
    }

    @Test
    public void shouldNotCallApiWhenCurrencyPairIsPresentInDbAndIsUpToDate() {
        //given
        final ExchangeInquiryDto dto = new ExchangeInquiryDto(EUR, CHF, 100.0);
        when(exchangeRateRepository.findByFromCurrencyAndToCurrency(EUR, CHF)).thenReturn(Optional.of(EXCHANGE_RATE));
        when(timeValidator.isTimeValid(EXCHANGE_RATE.getLastUpdate())).thenReturn(true);
        //when
        final Double result = exchangeService.getExchangeRate(dto);
        //then
        assertEquals(50.0, result, 0);
        verify(feed, never()).getExchangeRate(any());
        verify(exchangeRateRepository, never()).save(any());
    }

    @Test
    public void shouldCallApiWhenCurrencyPairIsPresentInDbAndIsOutdated() {
        //given
        when(exchangeRateRepository.findByFromCurrencyAndToCurrency(EUR, CHF)).thenReturn(Optional.of(EXCHANGE_RATE));
        when(timeValidator.isTimeValid(EXCHANGE_RATE.getLastUpdate())).thenReturn(false);
        when(feed.getExchangeRate(EXCHANGE_INQUIRY_DTO)).thenReturn(EXCHANGE_RATE_DTO);
        //when
        final Double result = exchangeService.getExchangeRate(EXCHANGE_INQUIRY_DTO);
        //then
        assertEquals(50.0, result, 0);
        verify(feed, times(1)).getExchangeRate(any());
        verify(exchangeRateRepository, times(1)).save(any());
    }
}