package com.conichi.bussines.feature.exchange.feed.io.exchangeratesapi;

import com.conichi.bussines.feature.exchange.dto.ExchangeInquiryDto;
import com.conichi.bussines.feature.exchange.dto.ExchangeRateDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EuropeanCentralBankFeedTest {

    private static final double RATIO = 0.4;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private EuropeanCentralBankFeed europeanCentralBankFeed;

    @Test
    public void shouldGetExchangeRate() {
        //given
        final ExchangeInquiryDto dto = new ExchangeInquiryDto("PLN", "CHF", 100.0);
        when(restTemplate.getForObject("https://api.exchangeratesapi.io/latest?base=PLN&symbols=CHF", FeedResponse.class)).thenReturn(createMockedResponse());
        //when
        final ExchangeRateDto result = europeanCentralBankFeed.getExchangeRate(dto);
        //then
        assertEquals(RATIO, result.getRatio(), 0);

    }

    private FeedResponse createMockedResponse() {
        return new FeedResponse(Collections.singletonMap("CHF", RATIO), "PLN", LocalDate.now());
    }
}