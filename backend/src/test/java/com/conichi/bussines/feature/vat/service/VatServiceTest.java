package com.conichi.bussines.feature.vat.service;

import com.conichi.bussines.feature.shared.TimeValidator;
import com.conichi.bussines.feature.vat.dto.VatDto;
import com.conichi.bussines.feature.vat.feed.VatFeed;
import com.conichi.bussines.feature.vat.model.Vat;
import com.conichi.bussines.feature.vat.repository.VatRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VatServiceTest {

    private static final String VAT_NUMBER = "PL123456";
    private static final VatDto VAT_DTO = VatDto.builder()
            .vatNumber(VAT_NUMBER)
            .companyAddress("")
            .companyName("")
            .countryCode("PL")
            .build();
    private static final Vat VAT = new Vat(1L, "PL", VAT_NUMBER, "", "", "", LocalDateTime.now());

    @Mock
    TimeValidator timeValidator;
    @Mock
    VatRepository vatRepository;
    @Mock
    VatFeed vatFeed;

    @InjectMocks
    VatService vatService;

    @Test
    public void shouldCallFeedWhenVatIsRequestedTheFirstTime() {
        //given
        when(vatRepository.findByCompleteVatNumber(VAT_NUMBER)).thenReturn(Optional.empty());
        when(vatFeed.getVatInfo(VAT_NUMBER)).thenReturn(VAT_DTO);
        //when
        VatDto result = vatService.findVatByCode(VAT_NUMBER);
        //then
        assertEquals(VAT_NUMBER, result.getVatNumber());
        verify(vatRepository, times(1)).save(any());
    }

    @Test
    public void shouldNotCallFeedWhenVatIsPresentInDbAndUpToDate() {
        //given
        when(vatRepository.findByCompleteVatNumber(VAT_NUMBER)).thenReturn(Optional.of(VAT));
        when(timeValidator.isTimeValid(VAT.getLastUpdate())).thenReturn(true);
        //when
        VatDto result = vatService.findVatByCode(VAT_NUMBER);
        //then
        assertEquals(VAT_NUMBER, result.getVatNumber());
        verify(vatFeed, never()).getVatInfo(any());
        verify(vatRepository, never()).save(any());
    }

    @Test
    public void shouldCallFeedWhenVatIsPresentInDbAndOutDated() {
        //given
        when(vatRepository.findByCompleteVatNumber(VAT_NUMBER)).thenReturn(Optional.of(VAT));
        when(timeValidator.isTimeValid(VAT.getLastUpdate())).thenReturn(false);
        when(vatFeed.getVatInfo(VAT_NUMBER)).thenReturn(VAT_DTO);
        //when
        VatDto result = vatService.findVatByCode(VAT_NUMBER);
        //then
        assertEquals(VAT_NUMBER, result.getVatNumber());
        verify(vatFeed, times(1)).getVatInfo(any());
        verify(vatRepository, times(1)).save(any());
    }
}