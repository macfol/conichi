package com.conichi.bussines.feature.exchange.service;

import com.conichi.bussines.configuration.ServiceTest;
import com.conichi.bussines.feature.exchange.dto.ExchangeInquiryDto;
import com.conichi.bussines.feature.exchange.exception.IncorrectCurrencyException;
import com.conichi.bussines.feature.exchange.model.ExchangeRate;
import com.conichi.bussines.feature.exchange.repository.ExchangeRateRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static com.conichi.bussines.configuration.TestDbInit.FROM_CURRENCY;
import static com.conichi.bussines.configuration.TestDbInit.TO_CURRENCY;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional
public class ExchangeServiceIntegrationTest extends ServiceTest {

    @Autowired
    private ExchangeRateRepository exchangeRateRepository;
    @Autowired
    private ExchangeService exchangeService;

    @Test
    public void shouldFailValidation() {
        //given
        final ExchangeInquiryDto inquiryDto = new ExchangeInquiryDto("AA", "bb", 100.0);

        //when, then
        assertThrows(IncorrectCurrencyException.class, () -> exchangeService.getExchangeRate(inquiryDto));
    }

    @Test
    public void shouldCalculateConvectionFromCache() {
        //given
        final ExchangeRate exchangeRateInDb = getExchangeFromDb();
        final Double amount = 100.0;
        final ExchangeInquiryDto dto = new ExchangeInquiryDto(FROM_CURRENCY, TO_CURRENCY + 1, amount);
        //when
        final Double result = exchangeService.getExchangeRate(dto);
        //then
        assertEquals(exchangeRateInDb.getRatio() * amount, result, 0);
        assertEquals(exchangeRateInDb.getLastUpdate(), getExchangeFromDb().getLastUpdate());
    }

    private ExchangeRate getExchangeFromDb() {
        return exchangeRateRepository.findByFromCurrencyAndToCurrency(FROM_CURRENCY, TO_CURRENCY + 1).get();
    }
}