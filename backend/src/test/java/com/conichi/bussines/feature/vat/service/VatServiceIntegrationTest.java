package com.conichi.bussines.feature.vat.service;

import com.conichi.bussines.configuration.ServiceTest;
import com.conichi.bussines.feature.vat.dto.VatDto;
import com.conichi.bussines.feature.vat.exception.InvalidVatCode;
import com.conichi.bussines.feature.vat.model.Vat;
import com.conichi.bussines.feature.vat.repository.VatRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static com.conichi.bussines.configuration.TestDbInit.COUNTRY_CODE;
import static com.conichi.bussines.configuration.TestDbInit.VAT_NUMBER_PREFIX;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional
public class VatServiceIntegrationTest extends ServiceTest {

    public static final String VAT_NUMER = COUNTRY_CODE + VAT_NUMBER_PREFIX + 1;

    @Autowired
    private VatRepository vatRepository;
    @Autowired
    VatService vatService;

    @Test
    public void shouldThrowExceptionWheUserInputIsIncorrect() {
        //given, when, then
        assertThrows(InvalidVatCode.class, () -> vatService.findVatByCode("A789"));
    }

    @Test
    public void shouldCalculateConvectionFromCache() {
        //given
        final Vat exchangeRateInDb = getExchangeFromDb();
        //when
        final VatDto result = vatService.findVatByCode(VAT_NUMER);
        //then
        assertEquals(exchangeRateInDb.getCountryCode(), COUNTRY_CODE);
        assertEquals(exchangeRateInDb.getLastUpdate(), getExchangeFromDb().getLastUpdate());
    }

    private Vat getExchangeFromDb() {
        return vatRepository.findByCompleteVatNumber(VAT_NUMER).get();
    }
}