package com.conichi.bussines.configuration;

import com.conichi.bussines.feature.vat.dto.VatDto;
import com.conichi.bussines.feature.vat.feed.VatFeed;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Source documentation: https://exchangeratesapi.io/
 */
@Service("MockedVatFeed")
@RequiredArgsConstructor
public class MockedVatFeed implements VatFeed {


    @Override
    public VatDto getVatInfo(final String vatNumber) {
        return VatDto.builder().build();
    }
}
