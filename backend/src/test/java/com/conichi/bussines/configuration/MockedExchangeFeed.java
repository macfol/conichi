package com.conichi.bussines.configuration;

import com.conichi.bussines.feature.exchange.dto.ExchangeInquiryDto;
import com.conichi.bussines.feature.exchange.dto.ExchangeRateDto;
import com.conichi.bussines.feature.exchange.feed.ExchangeFeed;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Source documentation: https://exchangeratesapi.io/
 */
@Service("MockedExchangeFeed")
@RequiredArgsConstructor
public class MockedExchangeFeed implements ExchangeFeed {

    public static final Double RATIO = 0.5;

    @Override
    public ExchangeRateDto getExchangeRate(final ExchangeInquiryDto exchangeInquiryDto) {
        return new ExchangeRateDto(exchangeInquiryDto.getFrom(), exchangeInquiryDto.getTo(), RATIO);
    }
}
