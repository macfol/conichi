package com.conichi.bussines.configuration;

import com.conichi.bussines.feature.exchange.model.ExchangeRate;
import com.conichi.bussines.feature.exchange.repository.ExchangeRateRepository;
import com.conichi.bussines.feature.vat.model.Vat;
import com.conichi.bussines.feature.vat.repository.VatRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
@Slf4j
public class TestDbInit {

    public static final int NUMBER_OF_TEST_ENTITIES = 5;
    public static final String FROM_CURRENCY = "EUR";
    public static final String TO_CURRENCY = "AB";

    public static final String COUNTRY_CODE = "PL";
    public static final String VAT_NUMBER_PREFIX = "123";

    @Autowired
    private ExchangeRateRepository exchangeRateRepository;
    @Autowired
    private VatRepository vatRepository;

    @PostConstruct
    public void initData() {
        log.info("Initializing test data");
        insertVat();
        insertExchanges();
    }

    private void insertVat() {
        final List<Vat> vatList = IntStream.range(0, NUMBER_OF_TEST_ENTITIES)
                .mapToObj(iteration -> Vat.builder()
                        .completeVatNumber(COUNTRY_CODE + VAT_NUMBER_PREFIX + iteration)
                        .companyAddress("address" + iteration)
                        .companyName("name" + iteration)
                        .vatNumber(VAT_NUMBER_PREFIX + iteration)
                        .countryCode(COUNTRY_CODE)
                        .build())
                .collect(Collectors.toList());

        vatRepository.saveAll(vatList);
    }

    private void insertExchanges() {
        final List<ExchangeRate> exchangeRateList = IntStream.range(0, NUMBER_OF_TEST_ENTITIES)
                .mapToObj(iteration -> ExchangeRate.builder()
                        .fromCurrency(FROM_CURRENCY)
                        .toCurrency(TO_CURRENCY + iteration)
                        .ratio(iteration * 0.5)
                        .build())
                .collect(Collectors.toList());

        exchangeRateRepository.saveAll(exchangeRateList);
    }
}
