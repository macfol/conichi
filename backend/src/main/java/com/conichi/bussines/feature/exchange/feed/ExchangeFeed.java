package com.conichi.bussines.feature.exchange.feed;

import com.conichi.bussines.feature.exchange.dto.ExchangeInquiryDto;
import com.conichi.bussines.feature.exchange.dto.ExchangeRateDto;

public interface ExchangeFeed {

    ExchangeRateDto getExchangeRate(final ExchangeInquiryDto exchangeInquiryDto);
}
