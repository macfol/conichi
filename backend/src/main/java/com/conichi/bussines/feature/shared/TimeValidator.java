package com.conichi.bussines.feature.shared;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TimeValidator {

    @Value("${feed.cache.max-hours}")
    private int maxTime;

    public boolean isTimeValid(LocalDateTime lastModifiedTime) {
        return lastModifiedTime.isAfter(LocalDateTime.now().minusHours(maxTime));
    }
}
