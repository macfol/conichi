package com.conichi.bussines.feature.exchange.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeInquiryDto {

    String from;
    String to;
    Double amount;
}
