package com.conichi.bussines.feature.exchange.feed.io.exchangeratesapi;

import com.conichi.bussines.feature.exchange.dto.ExchangeInquiryDto;
import com.conichi.bussines.feature.exchange.dto.ExchangeRateDto;
import com.conichi.bussines.feature.exchange.feed.ExchangeFeed;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Source documentation: https://exchangeratesapi.io/
 */
@Service("EuropeanCentralBank")
@RequiredArgsConstructor
public class EuropeanCentralBankFeed implements ExchangeFeed {

    private static final String TEMPLATE_URL = "https://api.exchangeratesapi.io/latest?base=%s&symbols=%s";

    private final RestTemplate restTemplate;

    @Override
    public ExchangeRateDto getExchangeRate(final ExchangeInquiryDto exchangeInquiryDto) {
        final String requestUri = createUrl(exchangeInquiryDto);
        final FeedResponse response = restTemplate.getForObject(requestUri, FeedResponse.class);

        return new ExchangeRateDto(exchangeInquiryDto.getFrom(), exchangeInquiryDto.getTo(), response.getRates().get(exchangeInquiryDto.getTo()));
    }

    private String createUrl(final ExchangeInquiryDto exchangeInquiryDto) {
        return String.format(TEMPLATE_URL, exchangeInquiryDto.getFrom(), exchangeInquiryDto.getTo());
    }
}
