package com.conichi.bussines.feature.vat.feed;

import com.conichi.bussines.feature.vat.dto.VatDto;

public interface VatFeed {

    VatDto getVatInfo(final String vatNumber);
}
