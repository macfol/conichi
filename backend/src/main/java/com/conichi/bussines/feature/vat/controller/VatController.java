package com.conichi.bussines.feature.vat.controller;

import com.conichi.bussines.feature.vat.dto.VatDto;
import com.conichi.bussines.feature.vat.service.VatService;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("vat")
@RequiredArgsConstructor
public class VatController {

    private final VatService vatService;

    @GetMapping("{code}")
    public VatDto findVatByCode(@ApiParam(required = true, defaultValue = "LU26375245") @PathVariable final String code) {
        return vatService.findVatByCode(code);
    }
}
