package com.conichi.bussines.feature.time;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;

@RestController
@RequestMapping("time")
public class TimeController {

    @GetMapping
    public ZonedDateTime getCurrentTime() {
       return ZonedDateTime.now();
    }
}
