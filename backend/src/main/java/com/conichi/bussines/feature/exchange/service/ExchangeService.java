package com.conichi.bussines.feature.exchange.service;

import com.conichi.bussines.feature.exchange.dto.ExchangeInquiryDto;
import com.conichi.bussines.feature.exchange.dto.ExchangeRateDto;
import com.conichi.bussines.feature.exchange.exception.IncorrectCurrencyException;
import com.conichi.bussines.feature.exchange.feed.ExchangeFeed;
import com.conichi.bussines.feature.exchange.model.ExchangeRate;
import com.conichi.bussines.feature.exchange.repository.ExchangeRateRepository;
import com.conichi.bussines.feature.shared.TimeValidator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ExchangeService {

    private final TimeValidator timeValidator;
    private final ExchangeFeed feed;
    private final ExchangeRateRepository exchangeRateRepository;

    public ExchangeService(final TimeValidator timeValidator, @Qualifier("exchangeFeedImplementation") final ExchangeFeed feed,
                           final ExchangeRateRepository exchangeRateRepository) {
        this.timeValidator = timeValidator;
        this.feed = feed;
        this.exchangeRateRepository = exchangeRateRepository;
    }

    public Double getExchangeRate(final ExchangeInquiryDto exchangeInquiryDto) {
        validateUserInput(exchangeInquiryDto);
        return exchangeRateRepository.findByFromCurrencyAndToCurrency(exchangeInquiryDto.getFrom(), exchangeInquiryDto.getTo())
                .map(entity -> calculateFromCashedResult(entity, exchangeInquiryDto))
                .orElseGet(() -> calculateFromFeed(exchangeInquiryDto));
    }

    private void validateUserInput(final ExchangeInquiryDto exchangeInquiryDto) {
        if (exchangeInquiryDto.getTo().length() != ExchangeRate.CURRENCY_ABBREVIATION_LENGTH ||
                exchangeInquiryDto.getFrom().length() != ExchangeRate.CURRENCY_ABBREVIATION_LENGTH) {
            throw new IncorrectCurrencyException("Invalid currency format");
        }
    }

    private Double calculateFromCashedResult(final ExchangeRate exchangeRate, final ExchangeInquiryDto exchangeInquiryDto) {
        if (timeValidator.isTimeValid(exchangeRate.getLastUpdate())) {
            return convert(exchangeRate, exchangeInquiryDto);
        }

        final ExchangeRateDto exchangeRateDto = callFeed(exchangeInquiryDto);
        updateEntity(exchangeRate, exchangeRateDto);
        return convert(exchangeRate, exchangeInquiryDto);
    }

    private Double calculateFromFeed(final ExchangeInquiryDto exchangeInquiryDto) {
        final ExchangeRateDto dto = callFeed(exchangeInquiryDto);
        final ExchangeRate exchangeRate = saveNewEntity(dto);
        return convert(exchangeRate, exchangeInquiryDto);
    }

    private Double convert(final ExchangeRate exchangeRate, final ExchangeInquiryDto exchangeInquiryDto) {
        return exchangeRate.getRatio() * exchangeInquiryDto.getAmount();
    }

    private void updateEntity(final ExchangeRate entity, final ExchangeRateDto exchangeRateDto) {
        entity.setRatio(exchangeRateDto.getRatio());
        exchangeRateRepository.save(entity);
    }

    private ExchangeRateDto callFeed(final ExchangeInquiryDto exchangeInquiryDto) {
        return feed.getExchangeRate(exchangeInquiryDto);
    }

    private ExchangeRate saveNewEntity(final ExchangeRateDto dto) {
        return exchangeRateRepository.save(ExchangeRateDto.parseFromDto(dto));
    }
}
