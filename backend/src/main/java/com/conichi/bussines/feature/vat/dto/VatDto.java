package com.conichi.bussines.feature.vat.dto;

import com.conichi.bussines.feature.vat.model.Vat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VatDto {

    @Builder.Default
    private Boolean valid = true;
    @JsonProperty("format_valid")
    @Builder.Default
    private Boolean formatValid = true;
    @JsonProperty("query")
    private String query;
    @JsonProperty("country_code")
    private String countryCode;
    @JsonProperty("vat_number")
    private String vatNumber;
    @JsonProperty("company_name")
    private String companyName;
    @JsonProperty("company_address")
    private String companyAddress;

    public static VatDto parseToDto(final Vat vat) {
        return VatDto.builder()
                .query(vat.getVatNumber())
                .countryCode(vat.getCountryCode())
                .vatNumber(vat.getVatNumber())
                .companyName(vat.getCompanyName())
                .companyAddress(vat.getCompanyAddress())
                .build();
    }

    public static Vat parseFromDto(final VatDto vatDto) {
        return Vat.builder()
                .countryCode(vatDto.getCountryCode())
                .vatNumber(vatDto.getVatNumber())
                .completeVatNumber(vatDto.getQuery())
                .companyName(vatDto.getCompanyName())
                .companyAddress(vatDto.getCompanyAddress())
                .build();
    }
}
