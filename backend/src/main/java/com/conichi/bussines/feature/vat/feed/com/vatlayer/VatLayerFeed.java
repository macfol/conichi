package com.conichi.bussines.feature.vat.feed.com.vatlayer;

import com.conichi.bussines.feature.vat.dto.VatDto;
import com.conichi.bussines.feature.vat.feed.VatFeed;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Source documentation: https://vatlayer.com/documentation
 */
@Service("VatLayer")
@RequiredArgsConstructor
public class VatLayerFeed implements VatFeed {

    private static final String URL_TEMPLATE = "http://apilayer.net/api/validate?access_key=%s&vat_number=";
    private final RestTemplate restTemplate;
    @Value("${feed.vat.key}")
    private String apiKey;

    @Override
    public VatDto getVatInfo(final String vatNumber) {
        final String url = String.format(URL_TEMPLATE, apiKey);
        return restTemplate.getForObject(url + vatNumber, VatDto.class);
    }
}
