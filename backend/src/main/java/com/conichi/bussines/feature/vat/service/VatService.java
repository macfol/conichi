package com.conichi.bussines.feature.vat.service;

import com.conichi.bussines.feature.shared.TimeValidator;
import com.conichi.bussines.feature.vat.dto.VatDto;
import com.conichi.bussines.feature.vat.exception.InvalidVatCode;
import com.conichi.bussines.feature.vat.feed.VatFeed;
import com.conichi.bussines.feature.vat.model.Vat;
import com.conichi.bussines.feature.vat.repository.VatRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VatService {

    private static final String VAT_PATTERN = "[A-Z]{2}[0-9]+";

    private final TimeValidator timeValidator;
    private final VatRepository vatRepository;

    private final VatFeed vatFeed;


    public VatService(final TimeValidator timeValidator, final VatRepository vatRepository,
                      @Qualifier("vatFeedImplementation") final VatFeed vatFeed) {
        this.timeValidator = timeValidator;
        this.vatRepository = vatRepository;
        this.vatFeed = vatFeed;
    }

    public VatDto findVatByCode(final String vatNumber) {
        validateUserInput(vatNumber);
        return vatRepository.findByCompleteVatNumber(vatNumber)
                .map(this::calculateFromCache)
                .orElseGet(() -> calculateFromFeed(vatNumber));
    }

    private VatDto calculateFromFeed(String vatNumber) {
        VatDto dto = vatFeed.getVatInfo(vatNumber);
        if (dto.getValid()) {
            saveNewEntity(dto);
        }
        return dto;
    }

    private VatDto calculateFromCache(final Vat entity) {
        if (timeValidator.isTimeValid(entity.getLastUpdate())) {
            return VatDto.parseToDto(entity);
        }

        final VatDto vatDto = vatFeed.getVatInfo(entity.getVatNumber());
        updateEntity(entity, vatDto);
        return vatDto;
    }

    private void updateEntity(final Vat entity, final VatDto vatDto) {
        entity.setCompanyAddress(vatDto.getCompanyAddress());
        entity.setCompanyName(vatDto.getCompanyName());
        vatRepository.save(entity);
    }

    private void saveNewEntity(final VatDto vatDto) {
        vatRepository.save(VatDto.parseFromDto(vatDto));
    }

    private void validateUserInput(final String code) {
        if (!code.matches(VAT_PATTERN)) {
            throw new InvalidVatCode();
        }
    }
}
