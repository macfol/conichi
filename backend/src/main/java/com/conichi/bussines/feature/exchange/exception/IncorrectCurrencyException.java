package com.conichi.bussines.feature.exchange.exception;

import com.conichi.bussines.config.BusinessException;

public class IncorrectCurrencyException extends BusinessException {

    public IncorrectCurrencyException(final String message) {
        super(message);
    }
}
