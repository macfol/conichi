package com.conichi.bussines.feature.exchange.dto;

import com.conichi.bussines.feature.exchange.model.ExchangeRate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeRateDto {

    String from;
    String to;
    double ratio;

    public static ExchangeRate parseFromDto(final ExchangeRateDto dto) {
        return ExchangeRate.builder()
                .fromCurrency(dto.getFrom())
                .toCurrency(dto.getTo())
                .ratio(dto.getRatio())
                .build();
    }
}
