package com.conichi.bussines.feature.exchange.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"fromCurrency", "toCurrency"}, name = "UNIQUE_CURRENCY_PAIR_CONSTRAINT")})
@EntityListeners(AuditingEntityListener.class)
public class ExchangeRate {

    public static final int CURRENCY_ABBREVIATION_LENGTH = 3;

    @Id
    @GeneratedValue
    private Long id;
    @Size(min = CURRENCY_ABBREVIATION_LENGTH, max = CURRENCY_ABBREVIATION_LENGTH)
    @Column(nullable = false)
    private String fromCurrency;
    @Size(min = CURRENCY_ABBREVIATION_LENGTH, max = CURRENCY_ABBREVIATION_LENGTH)
    @Column(nullable = false)
    private String toCurrency;
    @Column(nullable = false)
    @Min(0)
    private Double ratio;
    @LastModifiedDate
    private LocalDateTime lastUpdate;
}

