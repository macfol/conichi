package com.conichi.bussines.feature.exchange.feed.io.exchangeratesapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
class FeedResponse {

    private Map<String, Double> rates = new HashMap<>();
    private String base;
    private LocalDate date;
}
