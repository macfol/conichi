package com.conichi.bussines.feature.vat.exception;

import com.conichi.bussines.config.BusinessException;

public class InvalidVatCode extends BusinessException {
    public InvalidVatCode() {
        super("Invalid user input");
    }
}
