package com.conichi.bussines.feature.vat.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Vat {

    @Id
    @GeneratedValue
    private Long id;
    @NotEmpty
    @Size(max = 2, min = 2)
    @Column(nullable = false)
    private String countryCode;
    @NotEmpty
    @Column(nullable = false)
    private String vatNumber;
    @Column(nullable = false, unique = true)
    private String completeVatNumber;
    private String companyName;
    private String companyAddress;
    @LastModifiedDate
    @Column(nullable = false)
    private LocalDateTime lastUpdate;
}
