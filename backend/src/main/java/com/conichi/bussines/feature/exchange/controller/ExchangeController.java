package com.conichi.bussines.feature.exchange.controller;


import com.conichi.bussines.feature.exchange.dto.ExchangeInquiryDto;
import com.conichi.bussines.feature.exchange.service.ExchangeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("currency")
@RequiredArgsConstructor
public class ExchangeController {

    private final ExchangeService exchangeService;

    @PostMapping("convert")
    public Double getExchangeRate(@RequestBody final ExchangeInquiryDto exchangeInquiryDto) {
        return exchangeService.getExchangeRate(exchangeInquiryDto);
    }
}
