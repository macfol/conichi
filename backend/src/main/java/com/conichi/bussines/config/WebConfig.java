package com.conichi.bussines.config;

import com.conichi.bussines.feature.exchange.feed.ExchangeFeed;
import com.conichi.bussines.feature.vat.feed.VatFeed;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@RequiredArgsConstructor
public class WebConfig {

    private final ApplicationContext context;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ExchangeFeed exchangeFeedImplementation(@Value("${feed.exchange.implementation}") String qualifier) {
        return (ExchangeFeed) context.getBean(qualifier);
    }

    @Bean
    public VatFeed vatFeedImplementation(@Value("${feed.vat.implementation}") String qualifier) {
        return (VatFeed) context.getBean(qualifier);
    }
}
