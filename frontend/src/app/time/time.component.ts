import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

const BASE_URL = environment.baseUrl;

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css', '../app.component.css']
})
export class TimeComponent implements OnInit {

  date: Date;
  error: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  onGetTime(): void {
    this.http.get<Date>(`${BASE_URL}/time`).subscribe(result => {
      this.error = '';
      this.date = result;
    }, exception => {
      this.error = exception.message;
    });
  }
}
