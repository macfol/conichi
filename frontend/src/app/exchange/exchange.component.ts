import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ExchangeRequest} from './exchange-request';
import {ExchangeService} from './exchange.service';


@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.css']
})
export class ExchangeComponent implements OnInit {
  errorMessage: string;
  form: FormGroup;
  conversionResult: number;

  constructor(private service: ExchangeService) {
    this.prepareForm();
  }

  private prepareForm(): void {
    this.form = new FormGroup({
        from: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(3)]),
        to: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(3)]),
        amount: new FormControl(1, [Validators.required, Validators.min(1)]),
      },
    );
  }

  ngOnInit() {
  }

  onConvert(): void {
    const request: ExchangeRequest = this.prepareRequest();
    this.service.getExchange(request).subscribe(response => {
      this.conversionResult = response;
      this.errorMessage = '';
    }, errorResponse => this.errorMessage = errorResponse.error.message);
  }

  private prepareRequest(): ExchangeRequest {
    const request: ExchangeRequest = new ExchangeRequest();
    request.amount = this.form.get('amount').value;
    request.from = this.form.get('from').value.toUpperCase();
    request.to = this.form.get('to').value.toUpperCase();
    return request;
  }
}
