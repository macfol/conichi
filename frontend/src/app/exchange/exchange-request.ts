export class ExchangeRequest {

  from: string;
  to: string;
  amount: number;
}
