import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ExchangeRequest} from './exchange-request';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

const BASE_URL = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {

  constructor(private http: HttpClient) {
  }

  getExchange(request: ExchangeRequest): Observable<number> {
    return this.http.post<number>(`${BASE_URL}/currency/convert`, request);
  }
}
