import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TimeComponent} from './time/time.component';
import {ExchangeComponent} from './exchange/exchange.component';
import {VatComponent} from './vat/vat.component';


const routes: Routes = [
  {path: '', component: TimeComponent},
  {path: 'exchange', component: ExchangeComponent},
  {path: 'vat', component: VatComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
