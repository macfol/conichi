import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vat} from './vat';

const BASE_URL = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class VatService {

  constructor(private http: HttpClient) {
  }

  getInfo(code: string): Observable<Vat> {
    return this.http.get<Vat>(`${BASE_URL}/vat/${code}`);
  }
}
