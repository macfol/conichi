import { Component, OnInit } from '@angular/core';
import { Vat } from './vat';
import { VatService } from './vat.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-vat',
  templateUrl: './vat.component.html',
  styleUrls: ['./vat.component.css']
})
export class VatComponent implements OnInit {
  errorMessage: any;
  form: FormGroup;
  vatInfo: Vat;

  constructor(private service: VatService) {
    this.prepareForm();
  }

  private prepareForm(): void {
    this.form = new FormGroup({
      code: new FormControl('', [Validators.required, Validators.minLength(5)]),
    },
    );
  }

  ngOnInit(): void {
  }

  onRequest(): void {
    const code = this.form.get('code').value.toUpperCase();
    this.service.getInfo(code).subscribe(result => {
      this.proccessResult(result);
    }, errorResponse => this.errorMessage = errorResponse.error.message);
  }
  private proccessResult(result: Vat): void {
    if (result.valid) {
      this.vatInfo = result;
      this.errorMessage = '';
    } else {
      this.errorMessage = 'Number not found';
    }
  }
}
