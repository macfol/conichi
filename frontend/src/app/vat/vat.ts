export class Vat {
  company_address: string;
  company_name: string;
  country_code: string;
  format_valid: boolean;
  query: string;
  valid: boolean;
  vat_number: string;
}
